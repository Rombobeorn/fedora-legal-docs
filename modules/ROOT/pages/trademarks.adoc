////

Copyright Fedora Project Authors.

SPDX-License-Identifier: CC-BY-SA-4.0

////

= Fedora trademark guidelines

This current Fedora trademark guidelines are available at https://fedoraproject.org/wiki/Legal:Trademark_guidelines 

This page is a copy of that page, for convenience, but links here may not work, so please refer to the wiki page until the trademark guidelines and this page are updated.

== Use by Red Hat 

Fedora®, the Fedora word design, the Infinity design logo, Fedora Remix, and the Fedora Remix 
word design, either separately or in combination, are hereinafter referred to as "Fedora Trademarks" and are 
trademarks of Red Hat, Inc.  Except as provided in these guidelines, you may not use the Fedora Trademarks or 
any confusingly similar mark as a trademark for your product, or use the Fedora Trademarks in any other manner 
that might cause confusion in the marketplace, including but not limited to in advertising, on auction sites, 
or on software or hardware.  Red Hat protects the Fedora Trademarks on behalf of the entire Fedora community.  
Indeed, the law obligates trademark owners to police their marks and prevent the use of confusingly similar 
names by third parties.  As the trademark owner, Red Hat strives to use the Fedora Trademarks under the same 
guidelines as the rest of the community.

== Use by the Fedora Council

Red Hat enlists the assistance of the Fedora Council (hereinafter "Council") to oversee the trademark 
guidelines to ensure compliance by the community.  The Council may grant permission for uses as explained 
below.  The Council serves as the first line of mediation when questions of use arise.

== Secondary Mark

"Fedora Remix," whether in words or design (hereinafter "Secondary Mark") is one of the Fedora Trademarks, but 
with a special significance.  The Secondary Mark provides downstream distributors a means to indicate their 
work includes or is derived from Fedora content, and thereby drive more interest in the Fedora Project itself.

The Secondary Mark must be expressed as "Fedora Remix".  Guidelines for use of the Secondary Mark are laid out 
below in the sections where its use is permitted or appropriate.  Guidelines for presentation of the Secondary 
Mark are located [[Legal/Secondary_trademark_usage_guidelines | at this page]].

Use of the word "Fedora" in connection with the Secondary Mark must clearly indicate that the work contains 
modified Fedora content or non-Fedora content.  The notice may not contain phrases such as:

* "built from Fedora"
* "powered by Fedora"
* "contains Fedora"

The modified Fedora content should be referred to only as "Fedora Remix."  Other uses that do not properly 
indicate the work contains modified Fedora content or non-Fedora content are also not permitted.

Using the Secondary Mark also requires a statement in print or in another prominent location stating that end 
users are receiving modified Fedora software, and informing them where they ''can'' get unmodified Fedora 
software.

== Fedora Media

The Fedora Project distributes software in various formats, including a format which is designed to be 
reproduced on physical media by the recipient.  Hereinafter, Fedora software provided by the Fedora Project in 
such formats and unmodified reproductions thereof in any form are collectively referred to as "Fedora Media."  
Examples of Fedora Media include but are not limited to:

* installation CDs and DVDs,
* live bootable CDs, DVDs, and USB devices, and
* any copies of those media in a format suitable for data interchange.

== Usage That Does Not Require Permission

Below are some guidelines for use of the Fedora Trademarks where, as long as you are in compliance with the 
guidelines, no permission is necessary.  In all cases, use is permitted only provided that:

* the use is only in connection with promoting the Fedora Project or Fedora products
* the use is not disparaging to Red Hat, the Fedora Project or their products
* the use is appropriate for the mark used, for example, "Fedora Remix" is used only for remix products and "Fedora" alone is not used for remix products
* the use does not imply sponsorship or endorsement by Red Hat or the Fedora Project
* Proper trademark symbols are used in connection with the Fedora Trademarks and the trademark attribution statement must appear as explained in xref:_proper_trademark_use
* the [[Logo/UsageGuidelines | official logo usage guidelines]] are strictly observed

=== Community sites and accounts

Fedora defines community sites and accounts as pages related to the Fedora Project, but not officially 
maintained by Marketing team members or Social Media administrators. Often, these groups are specific to a 
region or country. These pages are usually run by an individual contributor or a group of contributors. These 
sites are non-commercial in nature (they are not selling a product).

Examples of these pages include (but are not limited to):

* placing the Fedora Trademarks on a personal web site or blog to support Fedora
* making a page on a social networking web service to support Fedora
* linking to Fedora from a wiki to provide information or show support for Fedora

While these online community sites and accounts are not officially endorsed by the Fedora Project or Red Hat, 
because they are part of the Fedora Community (and use the Fedora trademarks to identify themselves), their 
site owners, moderators, administrators, and users are required to comply with the 
[https://docs.fedoraproject.org/en-US/project/code-of-conduct/index.html Fedora Code of Conduct]. Community 
sites and accounts which are unable to meet this standard of conduct will be required to cease use of the 
Fedora trademarks, and will be not be promoted/advertised by Fedora. Our intent is for all Fedora community 
members to have a positive and welcoming experience in all community spaces, official and unofficial.

It is permissible to use the Fedora Trademarks on community sites and accounts to show your support for the 
Fedora Project, provided that:

* where possible, the design logo hyperlinks to the Fedora Project website, http://fedoraproject.org/, or if 
that is not possible, the site includes a prominent link to the Fedora Project website at 
http://fedoraproject.org/.

* the site indicates clearly that it is not affiliated with or endorsed by the Fedora Project; in addition, 
where possible:

** the site must include the text "This site is not affiliated with or endorsed by the Fedora Project" 
prominently on any page that includes the Fedora Trademarks, and

** if the Fedora Trademarks appear in a page header or any area that is designed to be presented on more than 
one page, the notice must also be designed to be presented on all of those pages as well.  (i.e., if the Fedora 
Trademarks appear in a site-wide header, the informational text must appear in that header or an identically 
site-wide footer.)

* the site does not use visual styling that could be confusing to viewers or visitors as to whether the site is 
hosted by or on behalf of the Fedora Project

=== Business web sites

In the past, community members have inquired whether it is permissible to show support for Fedora by:

* offering Fedora Media for sale at a retail business or web site
* displaying a link to the Fedora Project using the Fedora Trademarks from a business web site
* displaying the Fedora Trademarks as part of a business that provides services for clients using Fedora

The guidelines relating to such usage are set forth in this section.

It is permissible to use the Fedora Trademarks on business web sites, provided that:

* the web site has non-Fedora primary branding
* the design logo hyperlinks to the Fedora Project website, http://fedoraproject.org/ 
* the use does not imply sponsorship or endorsement by Red Hat or the Fedora Project
* the use of the Fedora Trademarks does not imply an association with or endorsement of any non-Fedora goods or services
* the site does not use visual styling that could be confusing to viewers or visitors as to whether the site is hosted by or on behalf of the Fedora Project

Provided these guidelines are observed, it is permissible to use the Fedora Trademarks to sell Fedora Media, and to advertise services for Fedora-based systems (installation, configuration, troubleshooting, etc.)

=== Business cards

In the past, community members have inquired whether it is permissible to use the Fedora Trademarks on business 
cards identifying the community member as a Fedora Ambassador.  The guidelines relating to such usage are set 
forth in this section.

It is permissible to use the Fedora Trademarks without prior permission on business cards to identify one's 
affiliation with the Fedora Project, provided the Fedora Ambassador Business card template located [[Business 
cards | at this URL]] is used.  This is the only business card use permitted; community members may not use any 
Fedora Trademarks or refer to the Fedora Project on any work-related business cards. Use of business contact 
information on the template is also frowned upon.

=== Promotional events 

In the past, community members have inquired whether it is permissible to use the Fedora Trademarks to promote free and open source software events such as a Linux installfest or community free software trade show.  The guidelines relating to such usage are set forth in this section.

It is permissible to use the Fedora Trademarks without prior permission to promote free and open source software events where individuals in the Fedora Project appear as Fedora Ambassadors, distribute Fedora Media, or otherwise represent the Fedora Project, provided the general guidelines above are followed. If you would like to make some non-software goods to give away at the event and don't already have a license to do so, see [[#Non-software goods | Non-software goods]].

=== Publications 

It is permissible to use the Fedora trademarks in the title and content of a publication, provided that:

* the use is clearly in reference to the Fedora Project or its software

* the use does not imply sponsorship or endorsement by Red Hat or the Fedora Project

* Proper trademark symbols are used in connection with the Fedora Trademarks and the trademark attribution 
statement must appear as explained in [[#Proper_Trademark_Use | Proper Trademark Use]].

=== Application themes, skins, and personas

In the past, community members have inquired whether it is permissible to show support for Fedora by placing 
the Fedora Trademarks in a theme, persona, or skin intended to alter the appearance of an application.  The 
guidelines relating to such usage are set forth in this section.

These guidelines do not apply to the appearance of a web site, which is covered elsewhere in this document.

It is permissible to use the Fedora Trademarks in themes, personas, or skins for applications to show your 
support for the Fedora Project, provided that:

* the use of the Fedora Trademarks does not conflict with the license or terms of use of the application being altered;

* the use is non-commercial in nature; and,

* the use does not imply sponsorship or endorsement by Red Hat or the Fedora Project.

If you wish to use any existing Fedora designs other than the Fedora Trademarks for your work, please observe 
the licensing requirements for those materials.  If you have questions about these requirements for any 
particular design, contact the [[Design|Fedora Design Team]].

=== Distributing Fedora software

==== Copies of unmodified Fedora media

In the past, community members have inquired whether it is permissible to use the Fedora Trademarks to label 
media made from exact copies of the CD and DVD images distributed by the Fedora Project and its mirrors.  The 
guidelines relating to such usage are set forth in this section.

It is permissible to use the Fedora Trademarks without prior permission to label Fedora Media, provided proper 
trademark symbols are used in connection with the Fedora Trademarks and the trademark attribution statement 
appears as explained in [[#Proper_Trademark_Use | Proper Trademark Use]].

The Fedora Project [https://fedoraproject.org/wiki/Artwork/MediaArt provides labels] in formats suitable for 
use in labeling Fedora Media, and recommends their use.

==== Distributing combinations of Fedora software with non-Fedora or modified Fedora software

In the past, community members have inquired whether it is permissible to use any identifiable Fedora branding 
to label media that contains modified Fedora software, or combinations of Fedora software with non-Fedora 
software.  This is the category where use of the Secondary Mark is appropriate.  Guidelines relating to such 
usage of the Secondary Mark are set forth in this section.

Community members may use the [[#Secondary_Mark | Secondary Mark]] on and in conjunction with media that 
commingles unmodified Fedora software with other content that is not included in the official Fedora 
repositories (hereinafter "Other Content"), or that provides modified Fedora software, provided that:

* the ''fedora-logos'', ''fedora-release'', and ''fedora-release-notes'' RPM packages are removed, although you 
may replace these with packages of your own devising not containing the Fedora trademarks;

* a notice is prominently displayed either on the physical media or, if the media is provided through 
electronic means, at the point of dissemination, indicating that:

** the software provided is not provided or supported by the Fedora Project, and

** official Fedora software is available through the Fedora Project website, and linking to the Fedora Project 
website at http://fedoraproject.org/

==== OEM pre-loads of unmodified Fedora software

In the past, community members have inquired whether it is permissible to use the Fedora Trademarks to label or 
advertise computer systems pre-loaded with Fedora software.  The guidelines relating to such usage are set 
forth in this section.

It is permissible to use the Fedora Trademarks without prior permission in connection with the provision and 
sale of computer systems pre-loaded with Fedora software, provided that:

* any web site advertising such systems follow the guidelines herein pertaining to [[#Business_web_sites | 
Business Web Sites]];

* the use does not imply sponsorship or endorsement by Red Hat or the Fedora Project;

* it is clear to the consumer of the computer system that there is no support available from the Fedora 
community other than that which is offered to all users;

* the use of the Fedora Trademarks does not imply an association with or endorsement of any non-Fedora goods or 
services;

* any non-Fedora content included as part of the install image must reside entirely within a single folder 
under /opt (e.g. /opt/VENDORNAME). Please note that this exception only applies to content, not software. If 
any non-Fedora or modified Fedora software is pre-loaded (or non-Fedora content is included outside of a single 
folder under /opt), refer to 
[[#OEM_pre-loads_of_combinations_of_Fedora_software_with_non-Fedora_or_modified_Fedora_software | OEM pre-loads 
of combinations of Fedora software with non-Fedora or modified Fedora software]].

* proper trademark symbols are used in connection with the Fedora Trademarks and the trademark attribution 
statement must appear as explained in [[#Proper_Trademark_Use | Proper Trademark Use]]; and

* the [[Logo/UsageGuidelines | official logo usage guidelines]] are strictly observed.

==== Virtual images or appliances with unmodified Fedora software

In the past, community members have inquired whether it is permissible to use the Fedora Trademarks to label or 
advertise virtual images or appliance distributions pre-loaded with Fedora software.  The guidelines relating 
to such usage are set forth in this section.

It is permissible to use the Fedora Trademarks without prior permission in connection with the provision and 
sale of virtual images or appliance distributions pre-loaded with Fedora software, provided that:

* any web site advertising such systems follow the guidelines herein pertaining to [[#Business_web_sites | 
Business Web Sites]];

* the use does not imply sponsorship or endorsement by Red Hat or the Fedora Project;

* it is clear to the consumer of the computer system that there is no support available from the Fedora 
community other than that which is offered to all users;

* the use of the Fedora Trademarks does not imply an association with or endorsement of any non-Fedora goods or 
services;

* any non-Fedora content is not pre-installed in the virtual image or appliance files in which the Fedora 
operating system is pre-loaded, but instead provided by separate distribution.  If the non-Fedora content is 
pre-loaded, refer to 
[[#Virtual_images_or_appliances_with_combinations_of_Fedora_software_with_non-Fedora_or_modified_Fedora_software 
| Virtual images or appliances with combinations of Fedora software with non-Fedora or modified Fedora 
software]].

* proper trademark symbols are used in connection with the Fedora Trademarks and the trademark attribution 
statement must appear as explained in [[#Proper_Trademark_Use | Proper Trademark Use]]; and

* the [[Logo/UsageGuidelines | official logo usage guidelines]] are strictly observed.

== Usage That Requires Permission

Community members must obtain permission prior to using any of the Fedora Trademarks in the following 
situations.

=== Distributing Fedora software

==== New combinations of unmodified Fedora software

In the past, community members have inquired whether it is permissible to use Fedora Trademarks to identify new 
combinations of only software provided by official Fedora repositories.  The guidelines relating to such usage 
are set forth in this section.

It is permissible to use Fedora Trademarks on and in conjunction with media that includes ''only'' software 
included in the official Fedora repositories, provided that:

* use of the Fedora Trademarks is approved by the Fedora Council. The Council may ask to see the proposed 
designs of labeling, marketing and collateral material before approving the use.

* proper trademark symbols are used in connection with the Fedora Trademarks, and

* the trademark attribution statement appears as explained in [[#Proper_Trademark_Use | Proper Trademark Use]].

You may also consider distributing the new combination under the [[#Distributing combinations of Fedora 
software with non-Fedora or modified Fedora software | Secondary Mark]].

==== Unmodified Fedora software, with other content separately on same physical medium

In the past, community members have inquired whether it is permissible to use the Fedora Trademarks to identify 
media that contains only software from the official Fedora repositories, but also includes other content such 
as documents or media files in a separate area of the media for use by the recipient.  The guidelines relating 
to such usage are set forth in this section.

Community members may use Fedora Trademarks on and in conjunction with media that contains software included in 
the official Fedora repositories, along with Other Content, provided that:

* use of the Fedora Trademarks is approved by the Fedora Council.  The Council may ask to see the proposed 
designs of labeling, marketing and collateral material before approving the use.

* the Other Content does not substantially modify the content, configuration, or operation of the Fedora 
software without additional user intervention,

* the Other Content is labeled and distributed in a way that clearly indicates it is not part of the Fedora 
software provided on the media, and is not affiliated with, provided by, or supported by the Fedora Project,

* it is clear who is responsible for the Other Content

* proper trademark symbols are used in connection with the Fedora Trademarks as explained in 
[[#Proper_Trademark_Use | Proper Trademark Use]], and

* the [[Logo/UsageGuidelines | official logo usage guidelines]] are strictly observed.

==== OEM pre-loads of combinations of Fedora software with non-Fedora or modified Fedora software

In the past, community members have inquired whether it is permissible to use the Fedora Trademarks to label or 
advertise computer systems pre-loaded with combinations of Fedora software with non-Fedora software, or with 
modified Fedora software.  The guidelines relating to such usage are set forth in this section.

It is permissible to use the [[#Secondary_Mark | Secondary Mark]] without prior permission in connection with 
the provision and sale of computer systems pre-loaded with a combination of Fedora software and non-Fedora or 
modified Fedora software, provided that:

* the fedora-logos, fedora-release, and fedora-release-notes RPM packages are removed, although you may replace 
these with packages of your own devising not containing the Fedora trademarks;

* a notice is prominently displayed either on or alongside the physical media, indicating that:

** the software provided is not provided or supported by the Fedora Project, and

** official Fedora software is available through the Fedora Project website, and linking to the Fedora Project 
website at http://fedoraproject.org/

==== Virtual images or appliances with combinations of Fedora software with non-Fedora or modified Fedora software

In the past, community members have inquired whether it is permissible to use the Fedora Trademarks to label or 
advertise virtual images or appliance distributions pre-loaded with combinations of Fedora software with 
non-Fedora software, or with modified Fedora software.  The guidelines relating to such usage are set forth in 
this section.

It is permissible to use the [[#Secondary_Mark | Secondary Mark]] without prior permission in connection with 
the provision and sale of virtual images and appliance distributions pre-loaded with a combination of Fedora 
software and non-Fedora or modified Fedora software, provided that:

* the fedora-logos, fedora-release, and fedora-release-notes RPM packages are removed, although you may replace 
these with packages of your own devising not containing the Fedora trademarks;

* a notice is prominently displayed adjacent to the image file or with the appliance, indicating that:

** the software provided is not provided or supported by the Fedora Project, and

** official Fedora software is available through the Fedora Project website, and linking to the Fedora Project 
website at http://fedoraproject.org/

=== Domain names

In the past, community members have inquired whether it is permissible to use the Fedora Trademarks, including 
the word "Fedora," in an Internet domain name.

The Fedora Project has established the [[Local community domains | ''fedoracommunity.org'' domain]] so that 
local community sites can form without cost or excessive effort to that community.  Details about the process 
and benefits of a ''fedoracommunity.org'' domain are located on the [[Local community domains]] wiki page.

For any other use of the Fedora Trademarks in a domain name, please read the [[Local community domains]] page 
and contact the [[Council]].

=== Non-software goods

In the past, community members have inquired whether it is permissible to use Fedora Trademarks on non-software 
goods such as T-shirts, stickers, and pens.

Community members may request from the Fedora Council a license to use the Fedora trademarks on non-software 
related goods, services, or other entities.  The Council, or someone it delegates for the task, will ask to see 
the proposed designs before approving the use.

== Unapproved Use

The following uses of the Fedora Trademarks or Secondary Mark are not approved under any foreseeable circumstances.

* Violations of the Logo Usage Guidelines or Trademark Usage Guidelines

* Any use outside these guidelines not by explicit written permission

Except as set forth herein, the trademark owner retains and reserves all rights to the Fedora Trademarks and 
their use, including the right to modify these guidelines.

These guidelines may be amended from time to time at the discretion of the Council.

== Proper Trademark Use

One of the purposes of the Fedora® Project is to make a cutting-edge version of Linux® widely available in 
consistent format that the public can come to trust.  To achieve this purpose it is important that the 
technology can quickly be identified and that the recipient knows the technology they are receiving is the 
official and unmodified version.  Red Hat has chosen the Fedora® mark to identify this effort and is granting 
usage rights in the Fedora Trademarks as previously described in this document in order to assure widespread 
availability.

=== Trademark Usage Guidelines and Examples
[cols="1, 1, 1"]
|===
|Guideline |Acceptable |Unacceptable

|When using the Fedora Trademarks you must provide the proper trademark symbols and a trademark attribution statement. 
| Use Fedora&reg; for the first instance of the trademark, and include the statement "Fedora and the Infinity design logo are trademarks of Red Hat, Inc." 
| Never using the &reg; mark for Fedora, nor a trademark statement per the guidelines.

| Always distinguish trademarks from surrounding text with at least initial capital letters or in all capital letters. 
| Fedora, FEDORA 
| fedora, yourFedora

| Always use proper trademark form and spelling. 
| Fedora 
| FeDoRa, Fuhdoerah

| Never pluralize a trademark.  Never use "a" or "the" to refer to an instance of the trademark.  Always use a trademark as an adjective modifying a noun, or as a singular noun. 
| This is a Fedora system.<br/>Anyone can install Fedora. 
| I put a Fedora on my mom's computer. I have seventeen Fedoras running in my lab.

| Never use a trademark as a verb.  Trademarks are products or services, never actions. 
| Install Fedora on your computer. 
| Fedoraize your system today!

| Never use a trademark as a possessive.  Instead, the following noun should be used in possessive form or the sentence reworded so there is no possessive. 
| The Fedora desktop's interface is very clean. 
| Fedora's desktop interface is very clean.

| Never translate a trademark into another language. 
| Quiero instalar Fedora 9 en mi sistema. 
| Quiero instalar Sombrero 9 en mi sistema.

| Never use trademarks to coin new words or names. 
| ''N/A'' 
| Fedora Fashion for geeks; Fedorate.

| Never alter a trademark in any way including through unapproved fonts or visual identifiers. 
| Proper use of the Fedora logo 
| Putting a blue hat on top of the trademark

| Never use or register any trademarks that are confusingly similar to, or a play on, the word Fedora. 
| ''N/A'' 
| Fuhdora Project

| Never combine your company name with the Fedora name or use the Fedora name in a way that it could be perceived that Red Hat or the Fedora Project and your company have an organizational link such as a joint venture. 
| AcmeCo uses Fedora software on all its servers. 
| Bix Max servers are a partnership of Fedora and AcmeCo.

| Never use the Fedora Trademarks in a disparaging manner or in a manner that infringes Red Hat trademark rights or violates any federal, state, or international law. 
| ''N/A'' 
| Little cartoon boy micturating on Fedora logo, applying Fedora logo outside permitted uses

| Never use terminology that states or implies that the Fedora Project assumes any responsibility for the performance of your products and services. 
| AcmeCo uses Fedora software on all its servers. 
| Fedora runs AcmeCo's servers.

| Never abbreviate or use any Fedora Trademarks as an acronym. 
| The Fedora Users and Developers Conference is known as FUDCon. 
| FedUDCon, FEDORA (Fabulous Exchangeable Distribution Of Real Artistry)

| The Fedora Infinity design logo must be hyperlinked to http://fedoraproject.org/, in contexts where such a hyperlink is technically feasible. 
| Hyperlinking the logo where feasible 
| Not hyperlinking the logo where feasible
|===

=== Logo Usage Guidelines

For more information about the requirements for logo size, appearance, placement, and other considerations, 
refer to [[Logo/UsageGuidelines | the Logo/Usage Guidelines]] for the Fedora mark and the 
[[Legal/Secondary_trademark_usage_guidelines | Secondary Trademark Usage Guidelines]] for the Secondary Mark.

== Other Information

Red Hat, Inc. ("Red Hat") does not permit use of its registered
trademarks without permission. If you are aware of any such use, please
contact us at fedora-legal@redhat.com.

